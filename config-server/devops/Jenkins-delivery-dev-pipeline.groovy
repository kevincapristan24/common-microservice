node{

	def mvnHome
	
	stage('Preparation'){
		try{
			git url: 'https://gitlab.com/kevincapristan24/common-microservice.git', branch'feature/employee'
			mvnHome = tool 'M2'
		} catch (e) {
			notifyStarted("Error al clonar proyecto Business-Employee de GitLab")
			throw e
		}
	}
	
	stage('Test'){
		try{
			sh "'${mvnHome}/bin/mvn' test"
		} catch (e) {
			notifyStarted("Error al realizar Test al proyecto Business-Employee")
			throw e
		}
	}
	
	stage('Build'){
		try{
			sh "'${mvnHome}/bin/mvn' clean package -DskipTests"
		} catch (e) {
			notifyStarted("Error al construir el proyecto Business-Employee")
			throw e
		}
	}
	
	stage('Packaging'){
		try{
			archive 'target/*.jar'
		} catch (e) {
			notifyStarted("Error al empaquetar .JAR del proyecto Business-Employee")
			throw e
		}
	}
	
	stage('Deployment'){
		try{
			sh ''
		} catch (e) {
			notifyStarted("Error al desplegar  el proyecto Business-Employee")
			throw e
		}
	}
	
	stage('Notification'){
		notifyStarted("Tu codigo ha sido testeado y desplegado con exito!!")
	}
	

}

def notifyStarted(String message){
	slackSend(color: '#FFFF00', message: "${message}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")

}
